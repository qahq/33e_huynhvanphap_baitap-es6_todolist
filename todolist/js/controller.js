export let renderTask = (list) => {
  let taskCompleted = "";
  let taskUnCompleted = "";
  list.forEach(function (item) {
    let content = `
        <div>
        <tr>
        <td>${item.task}</td>
        <span onclick="checkTask('${item.id}')"  <i class="fa fa-check-circle" ></i></span>
        <span onclick="deleteTask('${item.id}')" > <i class="fa fa-trash-alt"></i></span>
        </tr>
        </div>
        `;
    if (item.statusTask == false) {
      taskUnCompleted += content;
    } else {
      taskCompleted += content;
    }
  });
  document.getElementById("todo").innerHTML = taskUnCompleted;
  if (taskCompleted == "") {
    return;
  }
  document.getElementById("completed").innerHTML = taskCompleted;
};

export let todoList = (data) => {
  let todoList = [];
  data.forEach((item) => {
    if (item.statusTask == false) {
      todoList.push(item);
    }
  });
  return todoList;
};

//Lấy thông tin từ form
export let layThongTinTuform = () => {
  let task = document.getElementById("newTask").value;
  let statusTask = false;
  return {
    task,
    statusTask,
  };
};
