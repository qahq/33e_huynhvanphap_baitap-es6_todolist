import { renderTask, layThongTinTuform, todoList } from "./controller.js";

let listToDo = [];
let BASE_URL = "https://62ff962c34344b6431fcdd0c.mockapi.io/";

//LOADING
var turnOnLoading = function () {
  document.getElementById("loading").style.display = "flex";
};

var turnOffLoading = function () {
  document.getElementById("loading").style.display = "none";
};

let renderToDoListService = function () {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todoList`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      renderTask(res.data);
      listToDo = todoList(res.data);
    })
    .catch(function (err) {
      turnOffLoading();
    });
};
renderToDoListService();

//Thêm task
function addTask() {
  let dataForm = layThongTinTuform();
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todoList`,
    method: "POST",
    data: dataForm,
  })
    .then(function (res) {
      turnOffLoading();
      renderToDoListService();
    })
    .catch(function (err) {
      turnOffLoading();
    });
}
window.addTask = addTask;

//Delete Task
function deleteTask(id) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todoList/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      turnOffLoading();
      //chạy lại renderToDoListService để render danh sách mới nhất
      renderToDoListService();
    })
    .catch(function (err) {
      turnOffLoading();
    });
}
window.deleteTask = deleteTask;

//Check task
let checkTask = (id) => {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todoList/${id}`,
    method: "GET",
  })
    .then((res) => {
      turnOffLoading();
      checkStatus(res.data, id);
    })
    .catch((err) => {
      turnOffLoading();
    });
};

window.checkTask = checkTask;
let checkStatus = (data, id) => {
  let dataTask = data;
  if (dataTask.statusTask == false) {
    dataTask.statusTask = true;
  } else {
    dataTask.statusTask = false;
  }
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todoList/${id}`,
    method: "PUT",
    data: dataTask,
  })
    .then((res) => {
      turnOffLoading();
      renderToDoListService();
    })
    .catch((err) => {
      turnOffLoading();
    });
};

//Sắp xếp thứ tự 
let sxTangDan= ()=>{
  turnOnLoading()
  listToDo.sort((a,b)=>{
    if(a.task < b.task){
      return -1;
    }
    return 0;
  })
  turnOffLoading()
  renderTask(listToDo)
}
window.sxTangDan=sxTangDan

let sxGiamDan= ()=>{
  turnOnLoading()
  listToDo.sort((a,b)=>{
    if(a.task > b.task){
      return -1;
    }
    return 0;
  })
  turnOffLoading()
  renderTask(listToDo)
}
window.sxGiamDan=sxGiamDan